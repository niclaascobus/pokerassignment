package poker.deck;

import poker.hand.Card;

public interface ShuffleDeck {

    public void shuffle(Card[] deck, int numberOfSuites, int numberOfRanks);
}
