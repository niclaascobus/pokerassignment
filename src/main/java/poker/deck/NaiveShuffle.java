package poker.deck;

import poker.hand.Card;

public class NaiveShuffle implements ShuffleDeck{
    public void shuffle(Card[] deck, int numberOfSuites, int numberOfRanks)  {
        // shuffle
        int a = numberOfRanks * numberOfSuites;
        for (int i = 0; i < a; i++) {
            int r = i + (int) (Math.random() * (a-i));
            Card temp = deck[r];
            deck[r] = deck[i];
            deck[i] = temp;
        }
    }
}
