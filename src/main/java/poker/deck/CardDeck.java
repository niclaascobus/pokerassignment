package poker.deck;

import poker.hand.Card;

public class CardDeck {

    private Card[] deck;
    private String[] SUITS = {
            "C", "D", "H", "S"
    };

    private String[] RANKS = {
            "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "J", "Q", "K", "A"
    };

    public void buildDeck() {
        // initialize deck
        int n = SUITS.length * RANKS.length;
        deck = new Card[n];
        for (int i = 0; i < RANKS.length; i++) {
            for (int j = 0; j < SUITS.length; j++) {
                deck[SUITS.length*i + j] = new Card(RANKS[i], SUITS[j]);
            }
        }
    }

    public void shuffle()    {
        ShuffleDeck shuffler = new NaiveShuffle();
        shuffler.shuffle(deck, SUITS.length, RANKS.length);
    }

    public Card[] getDeck()   {
        return deck;
    }
}
