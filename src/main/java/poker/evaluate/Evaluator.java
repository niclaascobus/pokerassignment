package poker.evaluate;

import poker.hand.Card;

public abstract class Evaluator {

    public abstract int evaluate(Card[] pokerHand);

}
