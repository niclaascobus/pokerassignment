package poker.evaluate;

import poker.hand.Card;

public class FiveCardEvaluator extends Evaluator{
    /*I have looked at quite a few Evaluators on the Internet.
    Could not find one that I could plug in easily.
    Will attempt this if required*/
    private static String STRAIGHT_FLUSH = "Straight Flush";
    private static String FOUR_OF_A_KIND = "Four of a Kind";
    private static String FULL_HOUSE = "Full House";
    private static String FLUSH = "Flush";
    private static String STRAIGHT = "Straight";
    private static String THREE_OF_A_KIND = "Three of a Kind";
    private static String TWO_PAIR = "Two Pair";
    private static String ONE_PAIR = "One Pair";
    private static String HIGH_CARDS = "High Cards";

    private static FiveCardEvaluator handEvaluator;

    @Override
    public int evaluate(Card[] pokerHand) {
        //not implemented for now
        System.out.println("Using FiveCardEvaluator for evaluation");
        int handStrength = 1;
        return handStrength;
    }

    public static Evaluator getEvaluator()    {
        if(handEvaluator == null)   {
            handEvaluator = new FiveCardEvaluator();
        }
        return handEvaluator;
    }
}
