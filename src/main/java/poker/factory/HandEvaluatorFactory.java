package poker.factory;

import poker.evaluate.BadugiEvaluator;
import poker.evaluate.Evaluator;
import poker.evaluate.FiveCardEvaluator;

public class HandEvaluatorFactory {

    public static final int FIVE_CARD_DRAW = 0;
    public static final int BADUGI = 1;

    public static Evaluator getEvaluator(int pokerVariant)  {
        Evaluator evaluator;
        switch (pokerVariant)   {
            case HandEvaluatorFactory.FIVE_CARD_DRAW:
                evaluator = FiveCardEvaluator.getEvaluator();
                break;
            case HandEvaluatorFactory.BADUGI:
                evaluator = BadugiEvaluator.getEvaluator();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + pokerVariant);
        }
        return evaluator;
    }

}
