package poker.deal;

import poker.deck.CardDeck;
import poker.hand.FiveCardHand;

public class Dealer {

    public static void deal(CardDeck shuffledDeck, FiveCardHand hand, int numberOfCards)   {
        for(int i = 0; i < numberOfCards; i++) {
            System.out.println("Dealing card " + (i+1));
            hand.getPokerHand()[i] = shuffledDeck.getDeck()[i];
        }
    }
}
