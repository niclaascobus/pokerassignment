package poker.facade;

import poker.deal.Dealer;
import poker.deck.CardDeck;
import poker.evaluate.Evaluator;
import poker.factory.HandEvaluatorFactory;
import poker.hand.FiveCardHand;

/*To change the App from a Console app to a Web app I would expose below methods as Restful endpoints*/
public class WebFacade {

    public FiveCardHand getPokerHand()    {
        CardDeck cardDeck = new CardDeck();
        cardDeck.buildDeck();
        cardDeck.shuffle();
        FiveCardHand pokerHand = new FiveCardHand();
        Dealer.deal(cardDeck, pokerHand, FiveCardHand.numberOfCards);
        return pokerHand;
    }

    public int evaluatePokerHand(FiveCardHand pokerHand)  {
        Evaluator evaluator = HandEvaluatorFactory.getEvaluator(HandEvaluatorFactory.FIVE_CARD_DRAW);
        return evaluator.evaluate(pokerHand.getPokerHand());
    }
}
