package pokerTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import poker.deck.CardDeck;
import poker.facade.WebFacade;
import poker.hand.Card;
import poker.hand.FiveCardHand;

import java.util.Arrays;

public class PokerTest {

    @Test
    public void testPokerFacadeGetHand()   {
        WebFacade facade = new WebFacade();
        FiveCardHand pokerHand = facade.getPokerHand();
        Assertions.assertTrue(pokerHand != null);
        Assertions.assertTrue(pokerHand.getPokerHand() != null);
        Assertions.assertTrue(pokerHand.getPokerHand().length == FiveCardHand.numberOfCards);
    }

    @Test
    public void testPokerFacadeEvaluate()   {
        WebFacade facade = new WebFacade();
        FiveCardHand pokerHand = facade.getPokerHand();
        Assertions.assertTrue(pokerHand != null);
        Assertions.assertTrue(pokerHand.getPokerHand() != null);
        Assertions.assertTrue(pokerHand.getPokerHand().length == FiveCardHand.numberOfCards);
        Assertions.assertTrue(facade.evaluatePokerHand(pokerHand) > 0);
    }

    @Test
    public void testCardDeck()  {
        CardDeck cardDeck = new CardDeck();
        cardDeck.buildDeck();
        //Selecting random card sample for comparison. Sure there is a better way but will start with this
        Card card2 = cardDeck.getDeck()[1];
        Card card15 = cardDeck.getDeck()[14];
        Card card23 = cardDeck.getDeck()[22];
        Card card38 = cardDeck.getDeck()[37];
        Card card49 = cardDeck.getDeck()[48];
        Card[] sampleBefore = new Card[]{card2, card15, card23, card38, card49};
        cardDeck.shuffle();
        //Select cards from same positions after shuffle
        Card card2AfterShuffle = cardDeck.getDeck()[1];
        Card card15AfterShuffle = cardDeck.getDeck()[14];
        Card card23AfterShuffle = cardDeck.getDeck()[22];
        Card card38AfterShuffle = cardDeck.getDeck()[37];
        Card card49AfterShuffle = cardDeck.getDeck()[48];
        Card[] sampleAfter = new Card[]{card2AfterShuffle, card15AfterShuffle, card23AfterShuffle, card38AfterShuffle, card49AfterShuffle};
        Assertions.assertFalse(Arrays.equals(sampleBefore, sampleAfter));
    }
}
